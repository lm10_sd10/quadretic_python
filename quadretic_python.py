import cmath
import math
while True:
	try:
		a=int(input("Enter Co-efficient of x\u00b2 :- "))
		b=int(input("Enter Co-efficient of x :- "))
		c=int(input("Enter Co-efficient of c :- "))
		if(a<0):
			ap=str(a)+"x\u00b2"
		elif (a==0):
			ap=""
		elif(a==1):
			ap=""+"x\u00b2"
		elif(a==-1):
			ap="-"	+"x\u00b2"	
		else:
			ap=str(a)+"x\u00b2"
		if(b<0):
			bp=str(b)+"x"
		elif (b==0):
			bp=""
		else:
			bp="+"+str(b)+"x"
		if(c<0):
			cp=str(c)
		elif (c==0):
			cp=""
		else:
			cp="+"+str(c)				
		print("Your Entered Equation Is : {0}{1}{2}=0".format(ap,bp,cp))
		dis1=b*b
		dis2=4*a*c
		try:
			discriminant=math.sqrt(dis1-dis2)
		except ValueError:
			discriminant=cmath.sqrt(dis1-dis2)	
		div=2*a
		bee=-(b)
		try:
			alpha=(bee+discriminant)/div
			beta=(bee-discriminant)/div
			print("Roots are {0} and {1}".format(alpha,beta))
			print("Press CTRL-C to exit....")
		except ZeroDivisionError:
			try:
				if c<0:
					cnew=abs(c)
				elif c==0:
					cnew=0
				else:
					cnew=-c	
				al=cnew/b	
				print("Value of x is {0}".format(al))
				print("Press CTRL-C to exit....")
			except ZeroDivisionError:
				print("OOPSS...The values you provide does not form a equation...You must provide both or any co-efficients of x or x\u00b2")
				print("Press CTRL-C to exit....")
	except ValueError:
		print("Please enter integers....All fields are required...For 1 type 1 and for 0 type 0...")
		print("TRY  AGAIN...")
		print("Press CTRL-C to exit....")	
	
		
